/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _VM_H_
#define _VM_H_

/*
 * VM system-related definitions.
 *
 * You'll probably want to add stuff here.
 */


#include <machine/vm.h>

/* Fault-type arguments to vm_fault() */
#define VM_FAULT_READ        0    /* A read was attempted */
#define VM_FAULT_WRITE       1    /* A write was attempted */
#define VM_FAULT_READONLY    2    /* A write to a readonly page was attempted*/

/*
 * And, we have 16Mb/PAGE_SIZE = 2^12 pages. Therefore, the number of coremap
 * entry is 2^12. For every physical pages, we need 4 bytes in coremap to
 * save its status. In total 2^14 bytes, which means we need 2^14/PAGE_SIZE = 4
 * pages for our coremap.
 */
#define CM_PAGES          4
#define CM_IS_FIXED       0x80000000
#define CM_IS_DIRTY       0x40000000
#define CM_ENDMARK        0x20000000

/*
 * We use top 10 bits to index page directory, and middle 10 bits to index pages.
 * So, there are 2^10 entries in both pg_dir and pg_idx
 */
#define PG_DIR_NUM        1024
#define PG_IDX_NUM        1024

/*
 * Masks of page tables
 */
#define PG_DIR_MASK     0xffc00000      /* index of pg_dir page table */
#define PG_IDX_MASK     0x003ff000      /* index of pg_idx page table */
#define VALID           0x80000000      /* whether page table is valid or not */
#define READABLE        0x40000000      /* whether page table is readable or not */
#define WRITABLE        0x20000000      /* whether page table is writable or not */
#define EXECUTABLE      0x10000000      /* whether page table is executable or not */
#define PG_MASK         0x0000ffff      /* get the address of next page table / page */

/*
 * Masks for TLB
 */
#define TLB_NO_CACHE    0x00000800      /* index of no_cache bit */
#define TLB_DIRTY       0x00000400      /* index of dirty bit */
#define TLB_VALID       0x00000200      /* index of valid bit */
#define TLB_GLOBAL      0x00000100      /* index of global bit */

/*
 * Coremap structure (32 bits)
 *   1 bit   1 bit   1 bit                   9 bits      20 bits
 *   fixed   dirty   last page (endmark)     unused      page of virtual address (high 20 bits)
 *
 * Index: page of physical address
 */
struct coremap {
    uint32_t entries[PAGE_SIZE];
};

/*
 * Each entry of page directory corresponds to one pg_idx struct
 *
 * page directory structure (32 bits)
 *   1 bit       1 bit       1 bit       1 bit       8 bits      20 bits
 *   valid       readable    writable    executable  unused      page of the corresponding pg_idx virtual address
 *
 * Index: index of second order page table
 */
struct pg_dir {
    uint32_t entries[PG_DIR_NUM];
};

/*
 * Each entry of page index corresponds to one page
 *
 * page index structure (32 bits)
 *    1 bit       1 bit       1 bit       1 bit       8 bits      20 bits
 *    valid       readable    writable    executable  unused      page of virtual address (high 20 bits)
 *
 * Index: page of virtual address
 */
struct pg_idx {
    uint32_t entries[PG_IDX_NUM];
};

/* Initialization function */
void vm_bootstrap(void);

/* Fault handling function called by trap code */
int vm_fault(int faulttype, vaddr_t faultaddress);

/* Allocate/free kernel heap pages (called by kmalloc/kfree) */
vaddr_t alloc_kpages(unsigned npages);
void free_kpages(vaddr_t addr);

/* TLB shootdown handling called from interprocessor_interrupt */
void vm_tlbshootdown_all(void);
void vm_tlbshootdown(const struct tlbshootdown *);

/* Set a dirty bit */
void set_dirty(uint32_t index);


#endif /* _VM_H_ */
