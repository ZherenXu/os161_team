/*
 * Driver code for airballoon problem
 */
#include <types.h>
#include <lib.h>
#include <thread.h>
#include <test.h>
#include <synch.h>

#define N_LORD_FLOWERKILLER 8
#define NROPES 16
static int ropes_left = NROPES;

/* Data structures for rope mappings */

/* Implement this! */
struct rope {
    struct lock *rope_lk;     // locks for all ropes
    int number;               // hook number
    volatile int stake;       // corresponding stake number
    volatile bool is_severed; // rope is severed or not?
};

/* Synchronization primitives */

/* Implement this! */
/* Main_lk is used for conditional variable as well as global value count
 * This lock ensure that main thread will exit after all other threads exit
 */
struct lock *main_lk;
struct cv *main_cv;

/* Start_lk is used for count how many "starting" messages were printed
 */
struct lock *start_lk;
struct cv *start_cv;
/* Count variable will count how many threads are done at first. Once all
 * threads were done, "done" messages are printed, and count variable
 * start to count how many "done" are printed.
 */
static int count = 0;
static int start = 0;
static struct rope *ROPE;

/*
 * Describe your design and any invariants or locking protocols
 * that must be maintained. Explain the exit conditions. How
 * do all threads know when they are done?
 */

static
void
dandelion(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
    
	kprintf("Dandelion thread starting\n");
    lock_acquire(start_lk);
    start++;
    cv_wait(start_cv, start_lk); // Wait all threads print "starting"
    lock_release(start_lk);
    // check are there any rope left
    while (ropes_left != 0){
        int try = random() % NROPES;
        lock_acquire(ROPE[try].rope_lk);
        //check whether the rope was severed
        if (!ROPE[try].is_severed) {
            ROPE[try].is_severed = true; // Sever rope
            kprintf("Dandelion severed rope %d\n", ROPE[try].number);
            ropes_left--; // Update ropes_left
        }
        lock_release(ROPE[try].rope_lk);
        thread_yield();
    }
    lock_acquire(main_lk);
    count++;
    cv_wait(main_cv, main_lk); // Wait other threads
    kprintf("Dandelion thread done\n");
    count--;
    lock_release(main_lk);
    thread_exit(); //I'm not sure do we need this.
}

static
void
marigold(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
    
	kprintf("Marigold thread starting\n");
    lock_acquire(start_lk);
    start++;
    cv_wait(start_cv, start_lk); // Wait all threads print "starting"
    lock_release(start_lk);
    
    // check are there any rope left
    while (ropes_left != 0){
        int try = random() % NROPES;
        lock_acquire(ROPE[try].rope_lk);
        //check whether the rope was severed
        if (!ROPE[try].is_severed) {
            ROPE[try].is_severed = true; // Sever rope
            kprintf("Marigold severed rope %d from stake %d\n", ROPE[try].number, ROPE[try].stake);
            ropes_left--; // Update ropes_left
        }
        lock_release(ROPE[try].rope_lk);
        thread_yield();
    }
    lock_acquire(main_lk);
    count++;
    cv_wait(main_cv, main_lk); // Wait other threads
    kprintf("Marigold thread done\n");
    count--;
    lock_release(main_lk);
    thread_exit();
}

static
void
flowerkiller(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
    
	kprintf("Lord FlowerKiller thread starting\n");
    lock_acquire(start_lk);
    start++;
    cv_wait(start_cv, start_lk); // Wait all threads print "starting"
    lock_release(start_lk);
    
    // check are there any rope left
    while (ropes_left != 0){
        int try = random() % NROPES;
        lock_acquire(ROPE[try].rope_lk);
        //check whether the rope was severed
        if (!ROPE[try].is_severed) {
            int new_stake = random() % NROPES;
            // If two stakes are same one, get new stake
            while (new_stake == ROPE[try].stake){
                new_stake = random() % NROPES;
            }
            kprintf("Lord FlowerKiller switched rope %d from stake %d to stake %d\n", ROPE[try].number, ROPE[try].stake, new_stake);
            ROPE[try].stake = new_stake; //must after kprintf
        }
        lock_release(ROPE[try].rope_lk);
        thread_yield();
    }
    lock_acquire(main_lk);
    count++;
    cv_wait(main_cv, main_lk); // Wait other threads
    kprintf("Lord FlowerKiller thread done\n");
    count--;
    lock_release(main_lk);
    thread_exit();
}

static
void
balloon(void *p, unsigned long arg)
{
	(void)p;
	(void)arg;
    
	kprintf("Balloon thread starting\n");
    lock_acquire(start_lk);
    start++;
    cv_wait(start_cv, start_lk); // Wait all threads print "starting"
    lock_release(start_lk);
    
    lock_acquire(main_lk);
    count++;
    cv_wait(main_cv, main_lk); // Wait other threads
    kprintf("Balloon freed and Prince Dandelion escapes!\n");
    kprintf("Balloon thread done\n");
    count--;
    lock_release(main_lk);
    thread_exit();
}


// Change this function as necessary
int
airballoon(int nargs, char **args)
{
	int err = 0, i;
    (void)nargs;
    (void)args;
    (void)ropes_left;
    (void)count;
    
    // Create new locks and conditional variable
    start_lk = lock_create("start_lk");
    start_cv = cv_create("start_cv");
    main_lk = lock_create("main_lk");
    main_cv = cv_create("main_cv");
    ROPE = kmalloc(sizeof(struct rope) * NROPES);
    for (i = 0; i < NROPES; i++){
        ROPE[i].rope_lk = lock_create("rope_lk");
        ROPE[i].number = i;
        ROPE[i].stake = i;
        ROPE[i].is_severed = false;
    }

	err = thread_fork("Marigold Thread",
			  NULL, marigold, NULL, 0);
	if(err)
		goto panic;

	err = thread_fork("Dandelion Thread",
			  NULL, dandelion, NULL, 0);
	if(err)
		goto panic;

	for (i = 0; i < N_LORD_FLOWERKILLER; i++) {
		err = thread_fork("Lord FlowerKiller Thread",
				  NULL, flowerkiller, NULL, 0);
		if(err)
			goto panic;
	}

	err = thread_fork("Air Balloon",
			  NULL, balloon, NULL, 0);
	if(err)
		goto panic;

	goto done;
panic:
	panic("airballoon: thread_fork failed: %s)\n",
	      strerror(err));

done:
    /* 3 here means 3 threads: dandelion, marigold, balloon
     * Main thread have to be yielded until all "starting"
     * messages are printed.
     */
    while(start != N_LORD_FLOWERKILLER + 3) thread_yield();
    lock_acquire(start_lk);
    cv_broadcast(start_cv, start_lk);
    lock_release(start_lk);

    /* Main thread have to be yielded until all other threads
     * are done
     */
    while(count != N_LORD_FLOWERKILLER + 3) thread_yield();

    /* Release the lock and allow other threads to print "done"
     * messages.
     */
    lock_acquire(main_lk);
    cv_broadcast(main_cv, main_lk);
    lock_release(main_lk);
    
    /* Count != 0 means that there still have some threads not
     * print "done" messages.
     */
    while(count != 0) thread_yield();
    kprintf("Main thread done\n");

    // Destroy locks
    lock_destroy(start_lk);
    lock_destroy(main_lk);
    cv_destroy(start_cv);
    cv_destroy(main_cv);
    for (i = 0; i < NROPES; i++){
        lock_destroy(ROPE[i].rope_lk);
    }
    kfree(ROPE); // Avoid memory leak
    
    // Reset values for mutiple uses
    ropes_left = NROPES;
    count = 0;
    start = 0;
    return 0;
}
