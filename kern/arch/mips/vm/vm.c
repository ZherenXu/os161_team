
#include <types.h>
#include <spinlock.h>
#include <vm.h>
#include <lib.h>
#include <kern/errno.h>
#include <mips/tlb.h>
#include <addrspace.h>
#include <array.h>
#include <current.h>
#include <proc.h>
#include <spl.h>
#include <syscall.h>

static struct coremap *cm;
static uint32_t first_page = 0;
static uint32_t free_page;
static uint32_t last_page;


void vm_bootstrap() {
    
    /* allocate physical memory for coremap */
    paddr_t cm_paddr = ram_stealmem(CM_PAGES);
    
    /* coremap is allocated between first_page and free_page
     * other memory is allocated between free_page and last_page
     */
    cm = (struct coremap *) PADDR_TO_KVADDR(cm_paddr);
    
    /* initialize the bounders of other free memory */
    free_page = ram_stealmem(0) / PAGE_SIZE;
    last_page = ram_getsize() / PAGE_SIZE;
    
    /* fill the coremap */
    for (uint32_t i = first_page; i < free_page; i++) {
        vaddr_t address_page = PADDR_TO_KVADDR(i * PAGE_SIZE) / PAGE_SIZE;
        cm->entries[i] = CM_IS_FIXED | address_page;
    }
}

int vm_fault(int faulttype, vaddr_t faultaddress) {
    (void) faulttype;
    
    /* Check error */
    if(faultaddress <= 0 || faultaddress >= 0x80000000){
        return EFAULT;
    }

    /* Get the first order page table entry */
    struct pg_dir *fault_dir = curproc->p_addrspace->pg_dir;              /* Find the page table */
    vaddr_t p1_idx = faultaddress / (PAGE_SIZE * PG_IDX_NUM);             /* Index of the page table */
    uint32_t p1_entry = fault_dir->entries[p1_idx];                       /* Find the specific entry */
    
    struct pg_idx *fault_idx;

    /* Get the page table. */
    if (p1_entry & VALID) {
        fault_idx = (struct pg_idx *)((faultaddress & PG_MASK) << 12);
    } else {
        /* Create a new pg_idx table */
        fault_idx = kmalloc(sizeof(struct pg_idx));
        if(fault_idx == NULL){
            return ENOMEM;
        }
        for(int i=0; i<PG_IDX_NUM; i++){
            fault_idx->entries[i] = 0;
        }

        /* Fill the pg_dir table */
        vaddr_t page = (vaddr_t) fault_idx / PAGE_SIZE;
        p1_entry = VALID | READABLE | WRITABLE | EXECUTABLE | page;
    }
    
    vaddr_t p2_idx = (faultaddress & PG_IDX_MASK) / PAGE_SIZE;
    uint32_t p2_entry = fault_idx->entries[p2_idx];
    
    uint32_t fault_page;

    // Get the faulting address' physical address
    if (p2_entry & VALID) {
        fault_page = p2_entry & PG_MASK;
    } else {
        fault_page = free_page;
        //fault_page = alloc_kpages(1);
        
        p2_entry = VALID | READABLE | WRITABLE | EXECUTABLE | fault_page;
    }
    
    /* Find the physical page */
    paddr_t p_page = KVADDR_TO_PADDR(fault_page * PAGE_SIZE) / PAGE_SIZE;

    /* Fill tlb table */
    uint32_t eh;
    uint32_t el;

    paddr_t p_page_addr = p_page << 12;
    
    /*
     * TLB structure (64 bits)
     *
     * High 32-bits:
     *  20 bits                         6 bit
     *  Virtual Page Frame Number       Address Space ID
     *
     * Low 32-bits:
     *  20 bits                         1 bit       1 bit   1 bit   1 bit
     *  Physical Page Frame Number      No-Cache    Dirty   Valid   Global
     *
     */
    
    eh = 0 | (faultaddress & PAGE_FRAME) | ((curproc->p_pid) << 6);
    el = 0 | p_page_addr | TLB_DIRTY | TLB_VALID;
    tlb_write(eh, el, 1);
    
    return 0;
}

vaddr_t alloc_kpages(unsigned npages) {
    
    /* Go over the coremap to find a free page */
    for (uint32_t alloc_start = free_page; alloc_start < last_page - npages; alloc_start++) {
        
        if (!(cm->entries[alloc_start] & CM_IS_FIXED)) {
            
            /* Find a contiguous memory */
            for (size_t offset = 0; offset <= npages; offset++) {
                if (offset == npages) {
                    uint32_t alloc_end = alloc_start + npages - 1;
                    
                    vaddr_t address_page;
                    for (uint32_t i = alloc_start; i <= alloc_end; i++) {
                        
                        /* mark as allocated */
                        address_page = PADDR_TO_KVADDR(i * PAGE_SIZE) / PAGE_SIZE;
                        cm->entries[i] = CM_IS_FIXED | address_page;
                    }
                    
                    /* Mark the last page */
                    cm->entries[alloc_end] = cm->entries[alloc_end] | CM_ENDMARK;
                    
                    /* Return the starting point of virtual address */
                    vaddr_t address = PADDR_TO_KVADDR(alloc_start * PAGE_SIZE);
                    return address;
                }
                
                /* The memory is not contiguous */
                if (cm->entries[alloc_start + offset] & CM_IS_FIXED) break;
            }
        }
    }
    return ENOMEM;
}

void free_kpages(vaddr_t addr) {
    
    uint32_t free_start_addr = KVADDR_TO_PADDR(addr);
    
    /* variable "free" refers to the page number that we are going to free */
    for (uint32_t free = free_start_addr / PAGE_SIZE; cm->entries[free] & CM_IS_FIXED; free++) {
        
        /* Reach the last page */
        if (cm->entries[free] & CM_ENDMARK) {
            cm->entries[free] = 0;
            return;
        }
        
        /* Clear the slot */
        cm->entries[free] = 0;
    }
}

void vm_tlbshootdown_all() {
}

void vm_tlbshootdown(const struct tlbshootdown *TLBshootdown){
    (void) TLBshootdown;
}

void set_dirty(uint32_t index){
    cm->entries[index] = cm->entries[index] | CM_IS_DIRTY;
}

