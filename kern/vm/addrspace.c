/*
 * Copyright (c) 2000, 2001, 2002, 2003, 2004, 2005, 2008, 2009
 *	The President and Fellows of Harvard College.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the University nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE UNIVERSITY AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE UNIVERSITY OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <addrspace.h>
#include <vm.h>
#include <proc.h>
#include <machine/tlb.h>
#include <spl.h>

/*
 * Note! If OPT_DUMBVM is set, as is the case until you start the VM
 * assignment, this file is not compiled or linked or in any way
 * used. The cheesy hack versions in dumbvm.c are used instead.
 */

struct addrspace *
as_create(void)
{
	struct addrspace *as;

	as = kmalloc(sizeof(struct addrspace));
	if (as == NULL) {
		return NULL;
	}

	/*
	 * Initialize as needed.
	 */
    as->pg_dir = kmalloc(sizeof(struct pg_dir));
    if(as->pg_dir == NULL){
        return NULL;
    }
    
    /* Fill everything 0 the page directory */
    for(uint32_t i=0; i<PG_DIR_NUM; i++){
        as->pg_dir->entries[i] = 0;
    }
    as->prepare_load = false;
    as->heap_start = 0;
    as->heap_end = 0;
	return as;
}

int
as_copy(struct addrspace *old, struct addrspace **ret)
{
	struct addrspace *newas;

	newas = as_create();
	if (newas==NULL) {
		return ENOMEM;
	}

    /* Copy the page directory for address space */
    for(uint32_t i=0; i<PG_DIR_NUM; i++){
        newas->pg_dir->entries[i] = old->pg_dir->entries[i];
        
        /* Allocate a new pg_idx page table if the entry of pg_dir page table is valid */
        if(old->pg_dir->entries[i] & VALID){
            
            struct pg_idx *pg_idx_new = kmalloc(sizeof(struct pg_idx));
            if(pg_idx_new == NULL){
                as_destroy(newas);
                return ENOMEM;
            }
            for(uint32_t i=0; i<PG_IDX_NUM; i++){
                pg_idx_new->entries[i] = 0;
            }
            
            /* Get the old pg_idx page table and copy to the new one */
            vaddr_t dir_pmask = old->pg_dir->entries[i] & PG_MASK;
            struct pg_idx *pg_idx_old = (struct pg_idx *)(dir_pmask * PAGE_SIZE);
            
            /* Copy the page indices */
            for(uint32_t j=0; j<PG_IDX_NUM; j++){
                pg_idx_new->entries[j] = pg_idx_old->entries[j];
                
                /* Copy the valid page table */
                if(pg_idx_old->entries[j] & VALID){
                    
                    /* Get the address of old pages */
                    vaddr_t idx_pmask = pg_idx_old->entries[j] & PG_MASK;
                    vaddr_t vaddr_old = idx_pmask * PAGE_SIZE;
                    
                    /* Get the address of new pages and put it to new pg_idx page table */
                    vaddr_t vaddr_new = alloc_kpages(1);
                    vaddr_t v_page_new = vaddr_new / PAGE_SIZE;
                    pg_idx_new->entries[j] = VALID |
                                            (pg_idx_old->entries[j] & READABLE) |
                                            (pg_idx_old->entries[j] & WRITABLE) |
                                            (pg_idx_old->entries[j] & EXECUTABLE) |
                                            v_page_new;
                    
                    /* Copying */
                    memcpy((void*)vaddr_new, (void*)vaddr_old, PAGE_SIZE);
                    
                    /* Set dirty state */
                    set_dirty(vaddr_old / PAGE_SIZE);
                    set_dirty(v_page_new);
                }
            }
            
            /* Get the new page number for pg_idx page table and put it to new pg_dir page table */
            uint32_t idx_page = (vaddr_t)pg_idx_new / PAGE_SIZE;
            newas->pg_dir->entries[i] = VALID |
                                        (old->pg_dir->entries[i] & READABLE) |
                                        (old->pg_dir->entries[i] & WRITABLE) |
                                        (old->pg_dir->entries[i] & EXECUTABLE) |
                                        idx_page;
        }
    }

	*ret = newas;
	return 0;
}

void
as_destroy(struct addrspace *as)
{
    /* Go through the pg_dir page table and to find allocated pg_idx page tables */
    for(uint32_t i=0; i<PG_DIR_NUM; i++){
        
        /* Free the corresponding pg_idx page table */
        if(as->pg_dir->entries[i] & VALID){
            
            /* Get the pg_idx page table address */
            vaddr_t dir_pmask = as->pg_dir->entries[i] & PG_MASK;
            struct pg_idx *pg_idx = (struct pg_idx *)(dir_pmask * PAGE_SIZE);
            
            /* Find the valid pages */
            for(uint32_t j=0; j<PG_IDX_NUM; j++){
                if(pg_idx->entries[j] & VALID){
                    
                    /* Get the address of valid pages */
                    vaddr_t idx_pmask = pg_idx->entries[j] & PG_MASK;
                    vaddr_t vaddr = idx_pmask * PAGE_SIZE;

                    /* Free the valid pages */
                    free_kpages(vaddr);
                }
            }
            
            /* Free the valid pg_idx page tables */
            kfree(pg_idx);
        }
    }
    /* Free the pg_dir page table and the struct itself */
    kfree(as->pg_dir);
	kfree(as);
}

void
as_activate(void)
{
	struct addrspace *as;

	as = proc_getas();
	if (as == NULL) {
		/*
		 * Kernel thread without an address space; leave the
		 * prior address space in place.
		 */
		return;
	}

    /* Disable interrupts on this CPU while frobbing the TLB. */
    int spl;
    spl = splhigh();

    for (int i=0; i<NUM_TLB; i++) {
        tlb_write(TLBHI_INVALID(i), TLBLO_INVALID(), i);
    }

    splx(spl);
}

void
as_deactivate(void)
{
	/*
	 * Write this. For many designs it won't need to actually do
	 * anything. See proc.c for an explanation of why it (might)
	 * be needed.
	 */
}

/*
 * Set up a segment at virtual address VADDR of size MEMSIZE. The
 * segment in memory extends from VADDR up to (but not including)
 * VADDR+MEMSIZE.
 *
 * The READABLE, WRITEABLE, and EXECUTABLE flags are set if read,
 * write, or execute permission should be set on the segment. At the
 * moment, these are ignored. When you write the VM system, you may
 * want to implement them.
 */
int
as_define_region(struct addrspace *as, vaddr_t vaddr, size_t sz,
		 int readable, int writeable, int executable)
{
    /* Calculate the number of pages for this region */
    size_t p_required = sz / PAGE_SIZE + 1;

    /* */
    vaddr_t end = vaddr + p_required * PAGE_SIZE;
    if(end > as->heap_start)
    {
        as->heap_start = end;
        as->heap_end = as->heap_start;
    }
    
    /* Align heap start and heap end */
    if(as->heap_start % PAGE_SIZE != 0){
        as->heap_start = (as->heap_start / PAGE_SIZE + 1) * PAGE_SIZE;
        as->heap_end = as->heap_start;
    }

    vaddr_t cur_vaddr = vaddr;
    for(size_t i = 0; i < p_required; i++)
    {
        //Get the page table for the virtual address.
        vaddr_t p1_idx = cur_vaddr / (PAGE_SIZE * PG_IDX_NUM);
        struct pg_idx *pg_idx;
        vaddr_t p2_idx = (cur_vaddr & PG_IDX_MASK) / PAGE_SIZE;

        if((struct pg_idx *)(as->pg_dir->entries[p1_idx] * PAGE_SIZE) == NULL){
            as->pg_dir->entries[p1_idx] = as->pg_dir->entries[p1_idx] | VALID;
            
            /* Allocate a second order page table if it does not exist */
            pg_idx = kmalloc(sizeof(struct pg_idx));
            if(pg_idx == NULL){
                return ENOMEM;
            }
            for(uint32_t i=0; i<PG_IDX_NUM; i++){
                pg_idx->entries[i] = 0;
            }
            
            /* Set the attributes */
            pg_idx->entries[p2_idx] = VALID | (cur_vaddr/PAGE_SIZE);

        }
        else{
            /* The second order page table should exist */
            KASSERT((struct pg_idx *)(as->pg_dir->entries[p1_idx] * PAGE_SIZE) != NULL);
            pg_idx =(struct pg_idx *)(as->pg_dir->entries[p1_idx] * PAGE_SIZE);
            
            /* Set the attributes */
            if(pg_idx->entries[p2_idx] & VALID){
                pg_idx->entries[p2_idx] = VALID | (cur_vaddr/PAGE_SIZE);
            }
        }
        
        /* Permission attributes */
        if(readable){
            pg_idx->entries[p2_idx] = pg_idx->entries[p2_idx] | READABLE;
        }
        if(writeable){
            pg_idx->entries[p2_idx] = pg_idx->entries[p2_idx] | WRITABLE;
        }
        if(executable){
            pg_idx->entries[p2_idx] = pg_idx->entries[p2_idx] | EXECUTABLE;
        }
        
        cur_vaddr += PAGE_SIZE;
    }

    return 0;
	//return EUNIMP;
}

int
as_prepare_load(struct addrspace *as)
{
    /* Change the permission as read-write */
    as->prepare_load = true;
	return 0;
}

int
as_complete_load(struct addrspace *as)
{
    /* Change back to the original permission */
	as->prepare_load = false;
	return 0;
}

int
as_define_stack(struct addrspace *as, vaddr_t *stackptr)
{

	(void)as;

	/* Initial user-level stack pointer */
	*stackptr = USERSTACK;
    
    
    

	return 0;

}
